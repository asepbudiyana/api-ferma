const db = require("../models");
const response = require('./response');

const Chatbot = db.chatbot;

exports.add_base_url = (req, res) => {
    // Save Checklist to Database
    Chatbot.create({
        base_url: req.body.base_url,
    })
    .then(data => {
        response.created("Base url chatbot was added successfully!", data, res);
    })
    .catch(err => {
        response.error(err.message, res);
    });
};

exports.get_base_url = (req, res) => {

    Chatbot.findOne({
        order: [
            ['id', 'DESC']
        ]})
    .then(data => {
        response.ok("Get Base url data chatbot successfully!", data, res);
    }) 
    .catch(err => {
        response.error(err.message, res);
    });
};