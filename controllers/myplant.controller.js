const db = require("../models");
const response = require('./response');

var jwt = require("jsonwebtoken");

const { Op } = require("sequelize");
const MyPlant = db.myplant;
const Plant = db.plant;
const Checklist = db.checklist;
const sequelize = db.sequelize;
const Category = db.category;
const ChecklistPack = db.checklist_pack;

exports.add_my_plant = (req, res) => {
    let token = req.headers["x-access-token"];
    let decoded = jwt.decode(token);
    const id = decoded.id;

    // Save Myplant to Database
    MyPlant.create({
        name: req.body.name,
        is_done: 0,
        user_id: id,
        plant_id: req.body.plant_id
    })
    .then(myplant => {
        ChecklistPack.findAll({
            // where: {
            //     plant_id:  req.body.plant_id
            // },
            attributes: ['title', 'description', 'time'],
        })
        .then(cp => {
            for (let i = 0; i < cp.length; i++)  {
                var time = cp[i].time
                var now = new Date();
                var year = now.getFullYear();
                var month = parseInt(now.getMonth() + 1);
                var date = now.getDate();
                var result = year + "-" + month + "-" + date + " " + time;

                cp[i].setDataValue("is_checked", 0);
                cp[i].setDataValue("date", result);
                cp[i].setDataValue("myplant_id", myplant.id);
            }

            cp_data = cp.map(function(item){ return item.toJSON() })
            Checklist.bulkCreate(cp_data)
            .then((data) => {
                response.created("MyPlant was added successfully!", myplant, res);
            })
            .catch((e) => {
                response.error(e.message, res);
            })
        })
        .catch(error => {
            response.error(error.message, res);
        });
        
    })
    .catch(err => {
        response.error(err.message, res);
    });
};

exports.get_my_plant = (req, res) => {
    let token = req.headers["x-access-token"];
    let decoded = jwt.decode(token);
    const apiId = decoded.id;

    MyPlant.findAll({
        where: {
            user_id: apiId,
        },
        attributes: {
            include: [
                [
                    sequelize.literal(`(
                        SELECT COUNT(*)
                        FROM checklists AS c
                        WHERE
                            c.myplant_id = myplants.id
                            AND
                            c.is_checked = true
                    )`),
                    'finish_task'
                ],
                [
                    sequelize.literal(`(
                        SELECT COUNT(*)
                        FROM checklists AS c
                        WHERE
                            c.myplant_id = myplants.id
                    )`),
                    'total_task'
                ],
            ]
        },
        include: [
            {
                model: Plant,
                as: "plant",
                required: true,
                include: [
                    {
                        model: Category,
                        as: "category",
                        required: true
                    },
                ]
            },
            {
                model: Checklist,
                where: {
                    [Op.or]: [
                        {  is_checked : false }, 
                        {  is_checked : null }
                    ]
                },
                required: false

            },
        ] 
    })
    .then(data => {
        for (let i = 0; i < data.length; i++)  {
            // To calculate the time difference of two dates
            var Difference_In_Time = (new Date()).getTime() - data[i].createdAt.getTime();
            var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
            var progress_count = Math.floor((Difference_In_Days / data[i].plant.total_days)  * 100)
            var progress = Math.min(progress_count, 100);
            data[i].setDataValue("progress", progress); 

            data[i].finish_task = parseInt(data[i].finish_task);
            data[i].total_task = parseInt(data[i].total_task);

            if (progress >= 100) {
                data[i].is_done = true;
                data[i].setDataValue("checklists", []); 
            }
        }
        response.ok("Get plant data successfully!", data, res);
    }) 
    .catch(err => {
        response.error(err.message, res);
    });
};

exports.delete_my_plant = (req, res) => {
    const id = req.params.id;
    
    MyPlant.destroy({
        where: {id: id}
    })
    .then(num => {
        if (num == 1) {
            response.ok("My Plant was delete successfully", {}, res);
        } else {
            response.notfound("My Plant not found!", res);
        }
    })
    .catch(err => {
        response.error("Could not delete myplant with id = " + id, res);
    });
}