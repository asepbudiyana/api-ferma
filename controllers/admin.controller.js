const AdminBro = require('admin-bro')
const db = require('../models')
const AdminBroSequelize = require('@admin-bro/sequelize')
const uploadFeature = require('@admin-bro/upload')

AdminBro.registerAdapter(AdminBroSequelize)
const contentNavigation = {
    name: 'content',
    icon: 'Accessibility',
}

const adminBro = new AdminBro ({
    // Databases: [db],
    resources:  [
        { 
            resource: db.activity, 
            options: { 
                navigation: contentNavigation
            } 
        },
        { 
            resource: db.admin, 
            options: { 
                navigation: contentNavigation
            } 
        },
        { 
            resource: db.article, 
            options: { 
                navigation: contentNavigation,
                properties: {
                    picture: {
                        components: {
                            list: AdminBro.bundle('./my-dashboard-component')
                        }
                    }
                }
            } 
        },
        { 
            resource: db.checklist, 
            options: { 
                navigation: contentNavigation
            } 
        },
        { 
            resource: db.category, 
            options: { 
                navigation: contentNavigation
            } 
        },
        { 
            resource: db.myplant, 
            options: { 
                navigation: contentNavigation
            } 
        },
        { 
            resource: db.plant, 
            options: { 
                navigation: contentNavigation,
                listProperties: ['plant_name', 'type_id', 'category_id', 'summary', 'growing', 'harvesting', 'picture', 'total_days'],
                properties: {
                    picture: {
                        isVisible: {
                            show: false
                        },
                        components: {
                            list: AdminBro.bundle('../views/picture-show-component'),
                            show: AdminBro.bundle('../views/picture-show-component')
                        }
                    },
                    harvesting: {
                        components: {
                            list: AdminBro.bundle('../views/description-show-component')
                        }
                    },
                    growing: {
                        components: {
                            list: AdminBro.bundle('../views/description-show-component')
                        }
                    },
                    summary: {
                        components: {
                            list: AdminBro.bundle('../views/description-show-component')
                        }
                    },
                }
            },
            features: [uploadFeature({
                provider: { local: { bucket: 'public/img' } },
                properties: {
                    key: 'picture.path',
                    bucket: 'picture.folder',
                    mimeType: 'picture.type',
                    size: 'picture.size',
                    filename: 'picture.filename',
                    file: 'picture',
                },
            })] 
        },
        { 
            resource: db.type, 
            options: { 
                navigation: contentNavigation
            } 
        },
        { 
            resource: db.user, 
            options: { 
                navigation: contentNavigation
            } 
        },
    ],
    // dashboard: {
    //     handler: async () => {
    //       return { some: 'output' }
    //     },
    //     component: AdminBro.bundle('./my-dashboard-component')
    // },
    rootPath: '/',
    logoutPath: '/logout',
    loginPath: '/login',
    branding: {
        companyName: 'Ferma',
    },
});

module.exports = adminBro