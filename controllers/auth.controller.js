const db = require("../models");
const response = require('./response');
const config = require("../config/auth.config");
const User = db.user;

// const Op = db.Sequelize.Op;
const { Op } = require("sequelize");

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.signup = (req, res) => {
    // Save User to Database
    User.create({
        email: req.body.email,
        name: req.body.name,
        username: req.body.username,
        password: bcrypt.hashSync(req.body.password, 8)
    })
    .then(user => {
        
        var token = jwt.sign({ id: user.id }, config.secret, {
            expiresIn: 86400 // 24 hours
        });

        data = {
            'token': token,
            'expires_in': 86400,
            'user': user 
        };

        response.created("User was registered successfully!", data, res);
    })
    .catch(err => {
        response.error(err.message, res);
    });
};

exports.signin = (req, res) => {
    User.findOne({
        where: {
            username: req.body.username
        }
    })
    .then(user => {
        if (!user) {
            response.notfound("User not found.", res);
        }

        var passwordIsValid = bcrypt.compareSync(
            req.body.password,
            user.password
        );

        if (!passwordIsValid) {
           response.unauthorized("Invalid Password!", res);
        }

        var token = jwt.sign({ id: user.id }, config.secret);

        data = {
            'token': token,
            'user': user 
        };

        response.ok("User was login successfully!", data, res);
    })
    .catch(err => {
        response.error(err.message, res);
    });
};

exports.profile = (req, res) => {
    let token = req.headers["x-access-token"];
    let decoded = jwt.decode(token);
    const id = decoded.id;

    User.findByPk(id)
    .then(data => {
        response.ok("Get profile data successfully!", data, res);
    })
    .catch(err => {
        response.error("User not found", res);
    });
};

exports.updateProfile = (req, res) => {
    let token = req.headers["x-access-token"];
    let decoded = jwt.decode(token);
    const id = decoded.id;
        
    User.update({
        email: req.body.email,
        name: req.body.name,
        username: req.body.username,
        password: bcrypt.hashSync(req.body.password, 8),
        location: req.body.location,
        profile_picture: req.file === undefined ? "" : req.file.filename,
    }, {
        where: { id: id }
    })
    .then(num => {
        if (num == 1) {
            User.findByPk(id)
            .then(data => {
                response.created("Profil was updated successfully", data, res);
            })
            .catch(err => {
                response.error("User not found", res);
            });
        } else {
            response.notfound("Cannot update profile, user not found", res);
        }
    })
    .catch(err => {
        response.error("Error updating profile", res);
    });
};  

