const db = require("../models");
const response = require('./response');
const { Op } = require("sequelize");

const Plant = db.plant;
const CropStatistic = db.crop_statistic;
const Type = db.type;
const Category = db.category;
const Article = db.article;

exports.add_plant = (req, res) => {
    // Save User to Database
    Plant.create({
        plant_name: req.body.plant_name,
        type_id: req.body.type_id,
        category_id: req.body.category_id,
        summary: req.body.summary,
        growing: req.body.growing,
        harvesting: req.body.harvesting,
        picture: req.file === undefined ? "" : req.file.filename,
        total_days: req.body.total_days,
    })
    .then(plant => {
        CropStatistic.create({
            germ_days_low: req.body.germ_days_low,
            germ_temperature_low: req.body.germ_temperature_low,
            growth_days_low: req.body.growth_days_low,
            height_low: req.body.height_low,
            ph_low: req.body.ph_low,
            spacing_low: req.body.spacing_low,
            temperature_low: req.body.temperature_low,
            width_low: req.body.width_low,
            germ_days_up: req.body.germ_days_up,
            germ_temperature_up: req.body.germ_temperature_up,
            growth_days_up: req.body.growth_days_up,
            height_up: req.body.height_up,
            ph_up: req.body.ph_up,
            spacing_up: req.body.spacing_up,
            temperature_up: req.body.temperature_up,
            width_up: req.body.width_up,
            plant_id: plant.id
        })
        .then(statistics => {
            response.created("Plant was added successfully!", plant, res);
        })
        .catch(err => {
            response.error(err.message, res);
        })
    })
    .catch(err => {
        response.error(err.message, res);
    });
};

exports.get_plant = (req, res) => {

    Plant.findAll({
        include: [
            {
                model: CropStatistic
            },
            {
                model: Category,
                as: "category",
                required: true
            },
            {
                model: Type,
                as: "type",
                required: true
            }
        ]
    })
    .then(data => {
        response.ok("Get plant data successfully!", data, res);
    }) 
    .catch(err => {
        response.error(err.message, res);
    });
};

exports.get_detail_plant = (req, res) => {
    const apiId = req.params.id;

    Plant.findByPk(apiId, {
        include: [
            {
                model: CropStatistic
            },
            {
                model: Type,
                as: "type",
                required: true
            }
        ]
    })
    .then(data => {
        response.ok("Get plant data successfully!", data, res);
    })
    .catch(err => {
        response.error("Plant not found", res);
    });
};

exports.getCategoryPlant = (req, res) => {

    Category.findAll({
        include: [
            {
                model: Article,
                as: "article",
                required: true
            },
        ]
    })
    .then(data => {
        response.ok("Get plant category successfully!", data, res);
    }) 
    .catch(err => {
        response.error(err.message, res);
    });
};

exports.getDetailCategory = (req, res) => {
    const apiId = req.params.id;

    Category.findByPk(apiId, {
        include: [
            {
                model: Article,
                as: "article",
                required: true
            },
        ]
    })
    .then(data => {
        response.ok("Get category plant data successfully!", data, res);
    })
    .catch(err => {
        response.error("Category plant not found", res);
    });
};


exports.createCategoryPlant = (req, res) => {
    Category.create({
        name: req.body.name,
        article_id: req.body.article_id,
    })
    .then(data => {
        response.created("Category was created successfully!", data, res);
    })
    .catch(err => {
        response.error(err.message, res);
    });
};

exports.updateCategoryPlant = (req, res) => {
    const id = req.params.id;
        
    Category.update({
        name: req.body.name,
        article_id: req.body.article_id,
    }, {
        where: { id: id }
    })
    .then(num => {
        if (num == 1) {
            Category.findByPk(id)
            .then(data => {
                response.created("Category was updated successfully", data, res);
            })
            .catch(err => {
                response.error("Category not found", res);
            });
        } else {
            response.notfound("Cannot update Category", res);
        }
    })
    .catch(err => {
        response.error("Error updating Category", res);
    });
};  

exports.deleteCategoryPlant = (req, res) => {
    const id = req.params.id;
    
    Category.destroy({
        where: {id: id}
    })
    .then(num => {
        if (num == 1) {
            response.ok("Category was delete successfully", {}, res);
        } else {
            response.notfound("Category not found!", res);
        }
    })
    .catch(err => {
        response.error("Could not delete category with id = " + id, res);
    });
}

exports.getSuggestion = (req, res) => {
    const avg_ph = req.body.avg_ph;
    const avg_space = req.body.avg_space;
    const avg_temperature = req.body.avg_temperature;
    
    CropStatistic.findAll({
        where: {
            [Op.and]: [
                { ph_low: { [Op.lte]: avg_ph } }, 
                { ph_up: { [Op.gte]: avg_ph } }
            ], 
            [Op.and]: [
                { spacing_low: { [Op.lte]: avg_space } }, 
                { spacing_up: { [Op.gte]: avg_space } }
            ], 
            [Op.and]: [
                { temperature_low: { [Op.lte]: avg_temperature } }, 
                { temperature_up: { [Op.gte]: avg_temperature } }
            ], 
        },
        include: [
            {
                model: Plant,
                as: "plant",
                required: true,
                include: [
                    {
                        model: Category,
                        as: "category",
                        required: true
                    },
                    {
                        model: Type,
                        as: "type",
                        required: true
                    }
                ]
            },
        ]
    })
    .then(data => {
        response.ok("Get suggestion plant successfully!", data, res);
    }) 
    .catch(err => {
        response.error(err.message, res);
    });
};

exports.getTypePlant = (req, res) => {

    Type.findAll({})
    .then(data => {
        response.ok("Get plant type successfully!", data, res);
    }) 
    .catch(err => {
        response.error(err.message, res);
    });
};

exports.getDetailType = (req, res) => {
    const apiId = req.params.id;

    Type.findByPk(apiId, {})
    .then(data => {
        response.ok("Get type plant data successfully!", data, res);
    })
    .catch(err => {
        response.error("Type plant not found", res);
    });
};

exports.createTypePlant = (req, res) => {
    Type.create({
        name: req.body.name,
    })
    .then(data => {
        response.created("Type was created successfully!", data, res);
    })
    .catch(err => {
        response.error(err.message, res);
    });
};

exports.updateTypePlant = (req, res) => {
    const id = req.params.id;
        
    Type.update({
        name: req.body.name,
    }, {
        where: { id: id }
    })
    .then(num => {
        if (num == 1) {
            Type.findByPk(id)
            .then(data => {
                response.created("Type was updated successfully", data, res);
            })
            .catch(err => {
                response.error("Type not found", res);
            });
        } else {
            response.notfound("Cannot update type", res);
        }
    })
    .catch(err => {
        response.error("Error updating profile", res);
    });
};  

exports.deleteTypePlant = (req, res) => {
    const id = req.params.id;
    
    Type.destroy({
        where: {id: id}
    })
    .then(num => {
        if (num == 1) {
            response.ok("Type was delete successfully", {}, res);
        } else {
            response.notfound("Type not found!", res);
        }
    })
    .catch(err => {
        response.error("Could not delete type with id = " + id, res);
    });
}

exports.updatePlant = (req, res) => {
    const id = req.params.id;
        
    Plant.update({
        plant_name: req.body.plant_name,
        type_id: req.body.type_id,
        category_id: req.body.category_id,
        summary: req.body.summary,
        growing: req.body.growing,
        harvesting: req.body.harvesting,
        picture: req.file === undefined ? "" : req.file.filename,
        total_days: req.body.total_days,
    }, {
        where: { id: id }
    })
    .then(num => {
        if (num == 1) {
            CropStatistic.update({
                germ_days_low: req.body.germ_days_low,
                germ_temperature_low: req.body.germ_temperature_low,
                growth_days_low: req.body.growth_days_low,
                height_low: req.body.height_low,
                ph_low: req.body.ph_low,
                spacing_low: req.body.spacing_low,
                temperature_low: req.body.temperature_low,
                width_low: req.body.width_low,
                germ_days_up: req.body.germ_days_up,
                germ_temperature_up: req.body.germ_temperature_up,
                growth_days_up: req.body.growth_days_up,
                height_up: req.body.height_up,
                ph_up: req.body.ph_up,
                spacing_up: req.body.spacing_up,
                temperature_up: req.body.temperature_up,
                width_up: req.body.width_up,
            }, {
                where: { plant_id: id}
            })
            .then(success => {
                if (success == 1) {
                    Plant.findByPk(id)
                    .then(data => {
                        response.created("Plant was updated successfully", data, res);
                    })
                    .catch(err => {
                        response.error(err.message, res);
                    });
                } else {
                    response.notfound("Cannot update crop statistics", res);
                }
            })
            .catch(error => {
                response.error(error.message, res);
            });
        } else {
            response.notfound("Cannot update plant", res);
        }
    })
    .catch(err => {
        response.error("Error updating plant", res);
    });
};

exports.deletePlant = (req, res) => {
    const id = req.params.id;
    
    Plant.destroy({
        where: {id: id}
    })
    .then(num => {
        if (num == 1) {
            response.ok("Plant was delete successfully", {}, res);
        } else {
            response.notfound("Plant not found!", res);
        }
    })
    .catch(err => {
        response.error("Could not delete plant with id = " + id, res);
    });
}