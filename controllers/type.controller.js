const db = require("../models");
const response = require('./response');

const Types = db.type;

exports.add_types = (req, res) => {
    // Save Checklist to Database
    Types.create({
        name: req.body.name,
    })
    .then(data => {
        response.created("Type was added successfully!", data, res);
    })
    .catch(err => {
        response.error(err.message, res);
    });
};
