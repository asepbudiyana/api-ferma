const db = require("../models");
const response = require('./response');
const Activity = db.activity;

exports.createActivity = (req, res) => {
    Activity.create({
        title: req.body.title,
        time: req.body.time,
        myplant_id: req.body.myplant_id,
    })
    .then(data => {
        response.created("Activity was created successfully!", data, res);
    })
    .catch(err => {
        response.error(err.message, res);
    });
};

exports.findAllActivity = (req, res) => {
    const myplantId = req.params.myplantId;

    Activity.findAll({ 
        where: {
            myplant_id: myplantId
        }     
    })
    .then(data => {
        response.ok("Get All activity success", data, res);
    })
    
    .catch(err => {
        response.error(err.message || "Some error occurred while retrieving articles.", res)
    });
};

exports.updateActivity = (req, res) => {
    const id = req.params.id;
        
    Activity.update({
        title: req.body.title,
        time: req.body.time,
    }, {
        where: { id: id }
    })
    .then(num => {
        if (num == 1) {
            Activity.findByPk(id)
            .then(data => {
                response.created("Activity was updated successfully", data, res);
            })
            .catch(err => {
                response.error("Activity not found", res);
            });
        } else {
            response.notfound("Cannot update activity", res);
        }
    })
    .catch(err => {
        response.error("Error updating profile", res);
    });
};  

exports.deleteActivity = (req, res) => {
    const id = req.params.id;
    
    Activity.destroy({
        where: {id: id}
    })
    .then(num => {
        if (num == 1) {
            response.ok("Activity was delete successfully", {}, res);
        } else {
            response.notfound("Activity not found!", res);
        }
    })
    .catch(err => {
        response.error("Could not delete activity with id = " + id, res);
    });
}
