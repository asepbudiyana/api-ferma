// my-dashboard-component.jsx
import { ApiClient } from 'admin-bro'
import { Box } from '@admin-bro/design-system'
import React, { useEffect, useState } from "react";

const api = new ApiClient()

const Dashboard = () => {
  const [data, setData] = useState({})

  useEffect(() => {
    api.getDashboard().then((response) => {
      setData(response.data)
    })
  }, [])

  return (
    <Box variant="grey">
      <Box variant="white">
        some: { data.some }
      </Box>
    </Box>
  )
}

const PropertyComponentShow = (props) => {
    const {record, property} = props
    // return (
    //     <Box>{record.params[property.name]}</Box>
    // )
    return (
        <div>
            <img src={record.params[property.name]} alt="picture" width="200"/>
        </div>    
    );
}

export default PropertyComponentShow