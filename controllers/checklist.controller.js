const db = require("../models");
const response = require('./response');

const Checklists = db.checklist;

exports.add_checklist = (req, res) => {
    // Save Checklist to Database
    Checklists.create({
        title: req.body.title,
        is_checked: 0,
        description: req.body.description,
        date: req.body.date,
        myplant_id: req.body.myplant_id
    })
    .then(data => {
        response.created("Checklist was added successfully!", data, res);
    })
    .catch(err => {
        response.error(err.message, res);
    });
};

exports.get_checklist = (req, res) => {
    const apiId = req.params.id_myplant;

    Checklists.findAll({
        where: {
            myplant_id: apiId,
        }
    })
    .then(data => {
        response.ok("Get checklist data successfully!", data, res);
    }) 
    .catch(err => {
        response.error("Plant not found", res);
    });
};

exports.update_checklist = (req, res) => {
    const id = req.params.id;
    
    Checklists.findByPk(id)
    .then(data => {
        Checklists.update({
            is_checked: !data.is_checked,
        }, {
            where: { id: id }
        })
        .then(num => {
            if (num == 1) {
                data.is_checked = !data.is_checked;
                response.created("Checklist was updated successfully", data, res);
            } else {
                response.notfound("Cannot update checklist", res);
            }
        })
        .catch(err => {
            response.error("Error updating checklist", res);
        });
    })
    .catch(err => {
        response.error("Checklist not found", res);
    });
};  