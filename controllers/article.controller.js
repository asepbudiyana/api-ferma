const db = require("../models");
const response = require('./response');
const { Op } = require("sequelize");
const Article = db.article;
const Admin = db.admin;

exports.findAllArticle = (req, res) => {
    const title = req.query.title;
    var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

    Article.findAll({ 
        where: condition,
        include: [{
            model: Admin,
            as: "author",
            required: true
        }]        
    })
    .then(data => {
        response.ok("Get All article success", data, res);
    })
    
    .catch(err => {
        response.error(err.message || "Some error occurred while retrieving articles.", res)
    });
};

exports.findAllCategory = (req, res) => {
    const title = req.query.title;
    var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

    Article.findAll({
        attributes: ['category'],
        group: ['category']
    })
    .then(data => {
        data = data.map(cat => cat.category )
        response.ok("Get All category article success", data, res);
    })
    
    .catch(err => {
        response.error(err.message || "Some error occurred while retrieving articles.", res)
    });
};

exports.getDetailArticle = (req, res) => {
    const id = req.params.id;

    Article.findByPk(id, {
        include: [{
            model: Admin,
            as: "author",
            required: true
        }]   
    })
    .then(data => {
        response.ok("Get Article data successfully!", data, res);
    })
    .catch(err => {
        response.error(err.message, res);
    });
};

exports.add_article = (req, res) => {
    // Save Checklist to Database
    Article.create({
        title: req.body.title,
        category: req.body.category,
        description: req.body.description,
        picture: req.file === undefined ? "" : req.file.filename,
        source: req.body.source,
        adminId: req.body.adminId,
    })
    .then(data => {
        response.created("Article was added successfully!", data, res);
    })
    .catch(err => {
        response.error(err.message, res);
    });
};

exports.updateArticle = (req, res) => {
    const id = req.params.id;
        
    Article.update({
        title: req.body.title,
        category: req.body.category,
        description: req.body.description,
        picture: req.file === undefined ? "" : req.file.filename,
        source: req.body.source,
        adminId: req.body.adminId,
    }, {
        where: { id: id }
    })
    .then(num => {
        if (num == 1) {
            Article.findByPk(id)
            .then(data => {
                response.created("Article was updated successfully", data, res);
            })
            .catch(err => {
                response.error("Article not found", res);
            });
        } else {
            response.notfound("Cannot update article", res);
        }
    })
    .catch(err => {
        response.error("Error updating profile", res);
    });
};  

exports.deleteArticle = (req, res) => {
    const id = req.params.id;
    
    Article.destroy({
        where: {id: id}
    })
    .then(num => {
        if (num == 1) {
            response.ok("Article was delete successfully", {}, res);
        } else {
            response.notfound("Article not found!", res);
        }
    })
    .catch(err => {
        response.error("Could not delete article with id = " + id, res);
    });
}