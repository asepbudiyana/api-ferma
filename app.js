var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors')

var indexRouter = require('./routes/index.routes');
var usersRouter = require('./routes/users.routes');

const db = require("./models");

var app = express();

app.use(cors())
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use('/public', express.static(path.join(__dirname, 'public')));
// app.use(express.static(path.join(__dirname, 'views')));

// app.get('/admin', function (req,res) {
//   res.sendFile(path.join(__dirname, 'views') + "/index.html");
// });

app.use('/', indexRouter);
app.use('/users', usersRouter);

require('./routes/auth.routes')(app);
require('./routes/plant.routes')(app);
require('./routes/article.routes')(app);
require('./routes/checklist.routes')(app);
require('./routes/myplant.routes')(app);
require('./routes/activity.routes')(app);
require('./routes/type.routes')(app);
require('./routes/chatbot.routes')(app);
// require('./routes/admin.routes')(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

db.sequelize.sync();

module.exports = app;
