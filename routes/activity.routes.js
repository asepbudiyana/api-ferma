const { authJwt } = require("../middleware");
const controller = require("../controllers/activity.controller");

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get(
        "/api/activity/:myplantId",
        [authJwt.verifyToken],
        controller.findAllActivity
    );

    app.post(
        "/api/activity",
        [authJwt.verifyToken],
        controller.createActivity
    );

    app.put(
        "/api/activity/:id",
        [authJwt.verifyToken],
        controller.updateActivity
    );

    app.delete(
        "/api/activity/:id",
        [authJwt.verifyToken],
        controller.deleteActivity
    );
};