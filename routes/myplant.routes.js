const { authJwt } = require("../middleware");
const controller = require("../controllers/myplant.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
      res.header(
          "Access-Control-Allow-Headers",
          "x-access-token, Origin, Content-Type, Accept"
      );
      next();
  });
  
  app.get("/api/myplant", 
    [authJwt.verifyToken],
    controller.get_my_plant
  );
  app.post(
    "/api/myplant", 
    [authJwt.verifyToken],
    controller.add_my_plant
  );
  app.delete(
    "/api/myplant/:id",
    [authJwt.verifyToken],
    controller.delete_my_plant
  );
};

