const { authJwt } = require("../middleware");
const controller = require("../controllers/article.controller");

const multer = require('multer')
const express = require('express')

const crypto = require('crypto');
const path = require('path');

module.exports = function(app) {
  app.use(express.static('public'));
  const uploadDir = '/images/article/';
  const storage = multer.diskStorage({
    destination: "./public"+uploadDir,
    filename: function (req, file, cb) {
      crypto.pseudoRandomBytes(16, function (err, raw) {
        if (err) return cb(err)  

        cb(null, raw.toString('hex') + path.extname(file.originalname))
      })
    }
  })

  const upload = multer({storage: storage, dest: uploadDir });

  app.use(function(req, res, next) {
      res.header(
          "Access-Control-Allow-Headers",
          "x-access-token, Origin, Content-Type, Accept"
      );
      next();
  });
  
  app.get("/api/article", controller.findAllArticle);
  app.post(
    "/api/article", 
    [
      upload.single('picture'),
    ],
    controller.add_article
  );
  app.get("/api/article/category", controller.findAllCategory);
  app.get(
    "/api/article/:id",
    controller.getDetailArticle
  );
  app.put(
    "/api/article/:id", 
    [
      authJwt.verifyToken,
      upload.single('picture'),
    ],
    controller.updateArticle
  );

  app.delete(
    "/api/article/:id",
    [authJwt.verifyToken],
    controller.deleteArticle
  );
};

