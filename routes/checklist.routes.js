const { authJwt } = require("../middleware");
const controller = require("../controllers/checklist.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
      res.header(
          "Access-Control-Allow-Headers",
          "x-access-token, Origin, Content-Type, Accept"
      );
      next();
  });
  
  app.get(
    "/api/checklist/:id_myplant", 
    [authJwt.verifyToken],
    controller.get_checklist
  );

  app.post(
    "/api/checklist", 
    [authJwt.verifyToken],
    controller.add_checklist
  );

  app.put(
    "/api/checklist/:id",
    [authJwt.verifyToken],
    controller.update_checklist
);
};
