const { authJwt } = require("../middleware");
const controller = require("../controllers/plant.controller");
const multer = require('multer')
const express = require('express')

const crypto = require('crypto');
const path = require('path');



module.exports = function(app) {
  app.use(express.static('public'));
  const uploadDir = '/img/';
  const storage = multer.diskStorage({
    destination: "./public"+uploadDir,
    filename: function (req, file, cb) {
      crypto.pseudoRandomBytes(16, function (err, raw) {
        if (err) return cb(err)  

        cb(null, raw.toString('hex') + path.extname(file.originalname))
      })
    }
  })

  const upload = multer({storage: storage, dest: uploadDir });

  app.use(function(req, res, next) {
      res.header(
          "Access-Control-Allow-Headers",
          "x-access-token, Origin, Content-Type, Accept"
      );
      next();
  });
  app.post(
    "/api/plant", 
    [
      authJwt.verifyToken,
      upload.single('picture'),
    ],
    controller.add_plant
  );
  app.get(
    "/api/plant",
    controller.get_plant
  );
  app.get(
    "/api/plant/:id",
    controller.get_detail_plant
  );
  app.get(
    "/api/plantCategory",
    controller.getCategoryPlant
  );
  app.get(
    "/api/plantCategory/:id",
    controller.getDetailCategory
  );
  app.post(
    "/api/plantCategory",
    [authJwt.verifyToken],
    controller.createCategoryPlant
  );
  app.put(
    "/api/plantCategory/:id",
    [authJwt.verifyToken],
    controller.updateCategoryPlant
  );
  app.delete(
    "/api/plantCategory/:id",
    [authJwt.verifyToken],
    controller.deleteCategoryPlant
  );
  app.post(
    "/api/plantSuggestion",
    controller.getSuggestion
  );
  app.get(
    "/api/plantType",
    controller.getTypePlant
  );
  app.get(
    "/api/plantType/:id",
    controller.getDetailType
  );
  app.post(
    "/api/plantType",
    [authJwt.verifyToken],
    controller.createTypePlant
  );
  app.put(
    "/api/plantType/:id",
    [authJwt.verifyToken],
    controller.updateTypePlant
  );
  app.delete(
    "/api/plantType/:id",
    [authJwt.verifyToken],
    controller.deleteTypePlant
  );
  app.put(
    "/api/plant/:id", 
    [
      authJwt.verifyToken,
      upload.single('picture'),
    ],
    controller.updatePlant
  );
  app.delete(
    "/api/plant/:id",
    [authJwt.verifyToken],
    controller.deletePlant
  );
};