const AdminBroExpress = require('@admin-bro/express')
const controller = require("../controllers/admin.controller");

module.exports = function(app) {

    const router = AdminBroExpress.buildRouter(controller)

    app.use(controller.options.rootPath, router);
}
