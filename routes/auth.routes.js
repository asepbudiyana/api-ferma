const { verifySignUp, authJwt } = require("../middleware");
const controller = require("../controllers/auth.controller");
const multer = require('multer')
const express = require('express')
const crypto = require('crypto');
const path = require('path');

module.exports = function(app) {
    app.use(express.static('public'));
    const uploadDir = '/images/user/';
    const storage = multer.diskStorage({
      destination: "./public"+uploadDir,
      filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {
          if (err) return cb(err)  

          cb(null, raw.toString('hex') + path.extname(file.originalname))
        })
      }
    })

    const upload = multer({storage: storage, dest: uploadDir });

    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });
  
    app.post(
        "/api/auth/register",
        [
            verifySignUp.checkDuplicateUsernameOrEmail
        ],
        controller.signup
    );

    app.post("/api/auth/login", controller.signin);

    app.get(
        "/api/auth/profile",
        [authJwt.verifyToken],
        controller.profile
    );

    app.put(
        "/api/auth/profile",
        [
            authJwt.verifyToken,
            upload.single('profile_picture'),
        ],
        controller.updateProfile
    );
};