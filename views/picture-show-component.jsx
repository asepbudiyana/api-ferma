import React from "react";

const PictureShow = (props) => {
    const {record, property} = props
    return (
        <div>
            <img src={record.params[property.name]} alt="picture" width="200"/>
        </div>    
    );
}

export default PictureShow