import React from "react";

const DescriptionShow = (props) => {
    const {record, property} = props
    return record.params[property.name].substring(0, 100) + " ...";
}

export default DescriptionShow