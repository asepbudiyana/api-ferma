const db = require("../models");
const Article = db.article;

db.sequelize.sync().then(() => {
  console.log('Seed Article Db');
  initial();
});

function initial() {
  Article.create({
    title: "Jahe Hias, Tanaman Obat Sekaligus Tanaman Hias",
    category: "Tanaman Hias",
    description: `
    Pertanianku— Jahe hias atau Zingiber spectabel memiliki nama popular, yaitu Golden Shampoo Ginger. Tanaman ini termasuk keluarga jahe-jahean dan banyak ditemukan di Kepulauan Peninsula. Kata zingiber berasal dari bahasa Sanskerta yang berarti tanduk lembu jantan. Tanaman ini bisa berfungsi sebagai tanaman hias atau tanaman obat. Tak heran, banyak yang membudidayakan jahe hias di pekarangan rumahnya.
    Sekilas, tanaman ini mirip dengan tanaman bangle hantu atau Zingiber ottensii yang sama-sama digunakan untuk bunga potong atau tanaman obat. Bangle hantu sendiri berkhasiat untuk mengatasi rasa gatal, meredakan gejala batuk, demam, dan mengobati sakit pinggang.
    Kandungan zat kimia utama di dalam jahe hias adalah trans-d-bergamotena. Famili Zingeraceae memang terkenal kaya akan terpenoid dan juga minyak esensial. Tanaman hias ini dapat berfungsi sebagai antibakteri, antioksidan, dan antifungi.
    Tanaman jahe hias terdiri atas batang berdaun yang tingginya bisa mencapai 2—3 m dan memiliki rhizome. Jahe hias memiliki rangkaian bunga dengan panjang tangkai yang bisa mencapai 50—75 cm, tetapi rata-rata panjang tangkainya hanya sekitar 30 cm.
    Rangkaian bunga tanaman ini muncul terpisah dari tunasnya. Tanaman juga memiliki braktea yang terletak saling overlap. Braktea tersebut akan mengalami perubahan warna seiring dengan bertambahnya usia dari warna kuning menjadi merah. Selain mengalami perubahan warna, braktea akan membentuk kantung yang berlendir bersamaan dengan terbentuknya bunga.
    Jahe hias sering digunakan sebagai salah satu komponen dalam bunga potong. Bagian tanaman yang digunakan untuk bunga potong adalah brakteanya.
    Kebanyakan orang menganggap bagian yang berwarna kuning kemerah-merahan merupakan kuncup bunga tanaman, padahal bagian tersebut dinamakan braktea. Braktea tersebut berdiameter 1,7 cm dengan panjang mencapai 6 cm. Kuncup braktea bisa makin berkembang dan akan menghasilkan bunga di basal braktea.
    Baca Juga:  Fruit Leather Mangga, Olahan Buah yang Menjanjikan
    Jahe hias dapat dibudidayakan di dataran tinggi dan dataran rendah. Tanaman hias ini membutuhkan naungan selama masa pertumbuhannya. Anda bisa memperbanyak atau menggunakan bibit tanaman yang berasal dari pemisahan rumpun dan setek batang.
    Tidak ada tindakan khusus untuk menanam tanaman hias ini, Anda hanya perlu menyiangi gulma yang tumbuh, melakukan pemupukan, pengairan, pemberian mulsa, sekam, dan jerami. Tanaman ini hanya memerlukan kondisi tanah dengan kandungan hara yang tinggi di sepanjang hidupnya.
    `,
    picture: "1606827214.jpg",
    source: "https://www.pertanianku.com/jahe-hias-tanaman-obat-sekaligus-tanaman-hias/",
    adminId: 1
  });
  Article.create({
    title: "Peran Penting Budidaya Tanaman Hias untuk Lingkungan",
    category: "Tanaman Hias",
    description: `
    Pertanianku — Budidaya tanaman memiliki fungsi tertentu untuk kehidupan manusia dalam memenuhi kebutuhan terhadap makanan sehari-hari, ataupun lingkungan. Namun bagaimana dengan budidaya tanaman hias? Apakah memiliki peran yang sama pentingnya dengan budidaya tanaman pangan? Berikut ini penjelasannya.
    Memperindah lingkungan
    Fungsi utama dari tanaman hias bagi lingkungan tentunya untuk memberikan kesan cantik atau indah bagi lingkungan itu sendiri. Lingkungan yang dipenuhi dengan tanaman hias tentunya akan lebih terkesan indah, nyaman, dan menyenangkan. Jadi, ini akan memberikan kolerasi antara aura positif dan keindahan yang alami, ketimbang lingkungan yang gersang.
    Menyimpan cadangan air
    Tanaman memang dikenal dengan manfaatnya yang banyak salah satunya adalah menyimpan cadangan air. Segala jenis air termasuk air hujan yang jatuh akan tersimpan di dalam akar mereka. Jadi, lahan yang digunakan untuk budidaya tanaman hias akan lebih subur dan lembap.
    Suplai oksigen
    Manfaat lain yang diberikan oleh tanaman hias sama dengan tanaman-tanaman pada umumnya, yakni memberikan suplai oksigen dan kaya akan udara bersih. Bayangkan saja bagaimana rasanya berada di lingkungan yang penuh dengan beragam jenis tanaman hias yang sangat indah hingga bisa memanjakan mata. Ini juga akan menyediakan udara bersih yang bebas dari beragam pencemaran.
    Atasi sakit kepala
    Sakit kepala, terutama migrain adalah sakit yang disebabkan oleh tingginya kandungan karbondioksida. Jadi, harus dinetralisir oleh tanaman yang bisa memberikan suplai oksigen lebih banyak. Nah, lingkungan yang memiliki banyak budidaya tanaman hias akan menjadi salah satu jawaban dari masalah ini nantinya.
    Mengatasi stres
    Seperti sebuah taman ajaib, sebuah lingkungan yang dipenuhi dengan tanaman hias akan menyediakan tempat indah yang memiliki udara segar. Bahkan, keindahan dari tanaman yang ada di lingkungan tersebut nantinya bisa menjadi salah satu faktor yang bisa meredakan dan mengurangi stres yang diderita oleh seseorang.    
    `,
    picture: "1606182804.jpg",
    source: "https://www.pertanianku.com/peran-penting-budidaya-tanaman-hias-untuk-lingkungan/",
    adminId: 1
  });
  Article.create({
    title: "Pepaya Solinda, Pepaya Kaya Vitamin C",
    category: "Tanaman Buah",
    description: `
    Pertanianku — Pepaya merupakan buah yang sering disajikan sebagai buah meja karena sering dimakan secara langsung tanpa diolah terlebih dahulu. Ada banyak varietas pepaya yang bisa dikonsumsi oleh masyarakat. Salah satu varietas yang wajib dicoba adalah pepaya solinda.
    Pepaya solinda mengandung vitamin C sebanyak 235 mg. Kadar vitamin C tersebut 2—3 kali lebih banyak daripada kadar vitamin C yang direkomendasikan untuk dikonsumsi setiap harinya.
    Kandungan protein di dalam pepaya solinda sangat mudah dicerna. Pepaya juga mengandung kalori yang relatif rendah, hanya sekitar 39 kkal/100 gram dan tidak mengandung kolesterol.
    Buah pepaya terkenal sebagai sumber fitonutrien, vitamin, serta mineral yang baik bagi tubuh. Tidak hanya itu, pepaya juga mengandung vitamin A, B1, B2, B5, E, dan K, likopen, serat, kalsium, potassium, folat, dan magnesium. Seluruh nutrisi yang terkandung di dalam pepaya sangat bermanfaat untuk kesehatan tubuh.
    Pepaya solinda merupakan pepaya yang dikembangkan oleh Badan Penelitian dan Pengembangan Pertanian (Balitbangtan) melalui Balai Penelitian Tanaman Buah (Balitbu). Keunggulan dari varietas ini adalah daging buah berwarna kuning cerah, panjang buah mencapai 20—28 cm, lingkar 22—33 cm, ketebalan daging mencapai 1,3—3,5 cm, dan bobotnya sebesar 500—1.500 gram.
    Produktivitas tanaman mencapai 66—88 buah per pohon. Saat sudah matang, pepaya solinda mengeluarkan aroma harum dengan tekstur daging buah yang agak kenyal. Soal rasa sudah tidak diragukan lagi, pepaya ini terasa manis dan menyegarkan.
    Pepaya solinda bisa ditanam di lahan marginal karena memiliki daya adaptasi yang baik terhadap cekaman kekeringan, tekstur tanah yang keras serta curah hujan tinggi. Buah ini bisa tumbuh pada semua jenis tanah. Namun, tipe tanah yang ideal untuk menanam pepaya adalah tanah tersebut mengandung bahan organik, drainase, dan aerasinya baik, serta pH tanah sekitar 6,5—7. Pepaya ini bisa ditanam di dataran rendah hingga dataran tinggi dengan ketinggian 600 mdpl.
    `,
    picture: "1606182901.jpg",
    source: "https://www.pertanianku.com/pepaya-solinda-pepaya-kaya-vitamin-c/",
    adminId: 1
  });
  Article.create({
    title: "Urban farming dan manfaatnya bagi kesehatan",
    category: "General",
    description: `
    Tidak hanya di pedesaan, kegiatan bertani ternyata juga dapat dilakukan di area perkotaan. Fenomena inilah yang dinamakan urban farming. Selain dapat memperoleh sayur dan buah dengan lebih mudah, bertani seperti ini memberikan manfaat kesehatan tersendiri.
    Selain itu, dengan melakukan urban farming, berarti Anda juga turut menjaga kelestarian lingkungan. Lokalisasi produksi sayur dan buah seperti pada konsep urban farming dapat menurunkan jumlah bahan bakar yang dibutuhkan untuk mendistribusikan, mengemas, dan menjual produk makanan tersebut.
    Popularitas urban farming di Indonesia
    Bisa dibilang, aktivitas urban farming adalah “angin segar” bagi penduduk di perkotaan. Bila sebelumnya mereka tidak memiliki lahan untuk bertani, maka kini semakin banyak cara untuk melakukannya. Ada yang menggunakan pot yang digantung, atau bahkan memanfaatkan halamannya yang sempit untuk menanam tanaman seperti cabai, tomat atau sawi.
    Karena hal inilah, tren urban farming di Indonesia pun semakin meningkat, terutama di kota besar seperti Jakarta. Beberapa kelas untuk bercocok tanam pun diselenggarakan menyusul ketertarikan orang untuk memiliki tanaman yang mendukung kebutuhan sehari-hari mereka.
    Dibandingkan dengan negara lain yang lebih dulu memulai, di Indonesia sendiri, urban farming masih sebatas tren gaya hidup. Hal ini berbeda dengan negara lain yang menyumbang 20-30 persen dari kebutuhan pangannya.
    Berdasarkan data yang diberikan oleh Association for Vertical Farming (AVF), New York bahkan telah dapat menghasilkan sekitar 200-220 ton daun basil setiap bulannya setelah menerapkan konsep urban farming ini. Selain dapat mendukung kebutuhan sehari-hari, melakukan urban farming ternyata juga memberikan manfaat positif bagi tubuh.
    Urban farming dan kesehatan tubuh
    Selain bermanfaat bagi lingkungan, urban farming juga memiliki manfaat terhadap kualitas bahan makanan yang disajikan. Pasar lokal berskala kecil yang berada di kota-kota besar memberikan kesempatan bagi para produsen untuk menjaga makanan yang diproduksi agar tetap segar dan bergizi. Sebab, waktu yang digunakan untuk mentransportasikan bahan makanan tersebut menjadi lebih pendek apabila ditumbuhkan di perkebunan atau pertanian lokal.
    Bila dilihat dari sudut pandang kesehatan masyarakat, terdapat angka kejadian malnutrisi dan gangguan kesehatan terkait pola makan lainnya di kota-kota besar.
    Dengan menerapkan urban farming dan membawa bahan makanan bergizi ke komunitas lokal, risiko kekurangan bahan pangan dan terjadinya berbagai penyakit bisa berkurang, termasuk penyakit jantung, obesitas dan diabetes.
    Dalam skala yang jauh lebih kecil, urban farming juga dapat dilakukan oleh para individu di halaman rumahnya. Dengan menanam di kebun sendiri, pengeluaran sehari-hari pun lebih ekonomis dan juga pengetahuan mengenai konsumsi makanan yang sehat juga semakin meningkat.
    Tak hanya itu, berkebun dan bercocok tanam juga dapat meningkatkan aktivitas fisik seseorang, melatih ketajaman pikiran, membantu memperoleh vitamin D bagi tubuh, meningkatkan daya tahan tubuh, dan meningkatkan kekuatan serta koordinasi tangan.
    Dengan banyaknya manfaat yang diperoleh dari urban farming di atas, kegiatan ini dapat menjadi salah satu kegiatan pengisi akhir pekan yang menyenangkan bagi Anda dan keluarga di rumah. Tunggu apa lagi? Yuk, mulai berkebun di rumah!
    `,
    picture: "1606876192.jpg",
    source: "https://www.klikdokter.com/info-sehat/read/3616250/urban-farming-dan-manfaatnya-bagi-kesehatan",
    adminId: 1
  });
}