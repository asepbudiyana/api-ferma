const db = require("../models");
const Category = db.category;

db.sequelize.sync().then(() => {
  console.log('Seed Category Db');
  initial();
});

function initial() {
  Category.create({
    name: "Hydroponic",
    article_id: 1
  });
  Category.create({
    name: "Aeroponic",
    article_id: 1
  });
  Category.create({
    name: "Aquaponic",
    article_id: 1
  });
}