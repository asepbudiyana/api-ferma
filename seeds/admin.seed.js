const db = require("../models");
const Admin = db.admin;
var bcrypt = require("bcryptjs");

db.sequelize.sync().then(() => {
  console.log('Seed Admin Db');
  initial();
});

function initial() {
  Admin.create({
    username: "Admin",
    password: bcrypt.hashSync("1234", 8)
  });
}