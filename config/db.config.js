require('dotenv/config')

let CONFIG_DB;

if (process.env.ENVIRONTMENT == 'development') {
  CONFIG_DB = {
    HOST: "localhost",
    USER: "root",
    PASSWORD: "",
    DB: "ferma_db",
    dialect: "mysql",
    dialectOptions: {},
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  };
} else {
  CONFIG_DB = {
    HOST: "ec2-34-195-233-155.compute-1.amazonaws.com",
    USER: "roqgrtimrpixkf",
    PASSWORD: "1040286db91e12d88be6ab550b7762bdaa952158ae1d3cc1c7259c4ec45081f6",
    DB: "d8d3m31sshcrr9",
    dialect: "postgres",
    dialectOptions: {
      ssl: {
        require: true,
        rejectUnauthorized: false
      }  
    },
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  };
}

module.exports = CONFIG_DB;