module.exports = (sequelize, Sequelize) => {
    const Category = sequelize.define("categories", {
        name: {
            type: Sequelize.STRING
        },
        article_id: {
            type: Sequelize.INTEGER,
        }
    });
    
    return Category;
};