module.exports = (sequelize, Sequelize) => {
    const Checklists = sequelize.define("checklists", {
        title: {
            type: Sequelize.STRING
        },
        is_checked: {
            type: Sequelize.BOOLEAN
        },
        description: {
            type: Sequelize.TEXT
        },
        date: {
            type: Sequelize.DATE
        },
        myplant_id: {
            type: Sequelize.INTEGER
        },
    });
    
    Checklists.prototype.toJSON =  function () {
        var values = Object.assign({}, this.get());
        
        return values;
    }

    return Checklists;
};