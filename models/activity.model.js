module.exports = (sequelize, Sequelize) => {
    const Activity = sequelize.define("activities", {
        title: {
            type: Sequelize.STRING
        },
        time: {
            type: Sequelize.DATE
        },
        myplant_id: {
            type: Sequelize.INTEGER,
        }
    }, {paranoid: true});
    
    Activity.prototype.toJSON =  function () {
        var values = Object.assign({}, this.get());
        
        return values;
    }

    return Activity;
};