module.exports = (sequelize, Sequelize) => {
    const MyPlants = sequelize.define("myplants", {
        name: {
            type: Sequelize.STRING
        },
        is_done: {
            type: Sequelize.BOOLEAN
        },
        user_id: {
            type: Sequelize.INTEGER
        },
        plant_id: {
            type: Sequelize.INTEGER
        }
    });
    
    MyPlants.prototype.toJSON =  function () {
        var values = Object.assign({}, this.get());
        
        return values;
    }

    return MyPlants;
};