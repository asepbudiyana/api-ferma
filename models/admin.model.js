module.exports = (sequelize, Sequelize) => {
    const Admin = sequelize.define("admins", {
        username: {
            type: Sequelize.STRING
        },
        password: {
            type: Sequelize.STRING
        },
    });
    
    Admin.prototype.toJSON =  function () {
        var values = Object.assign({}, this.get());
        
        delete values.password;
        return values;
    }

    return Admin;
};