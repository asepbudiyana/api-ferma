module.exports = (sequelize, Sequelize) => {
    const ChecklistPack = sequelize.define("checklist_packs", {
        title: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.TEXT
        },
        time: {
            type: Sequelize.TIME
        },
        plant_id: {
            type: Sequelize.INTEGER
        },
    });
    
    ChecklistPack.prototype.toJSON =  function () {
        var values = Object.assign({}, this.get());
        
        return values;
    }

    return ChecklistPack;
};