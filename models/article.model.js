const config = require("../config/app.config");

module.exports = (sequelize, Sequelize) => {
    const Article = sequelize.define("articles", {
        title: {
            type: Sequelize.STRING
        },
        category: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.TEXT
        },
        picture: {
            type: Sequelize.STRING
        },
        source: {
            type: Sequelize.TEXT
        },
        adminId: {
            type: Sequelize.INTEGER,
        }
    });
    
    Article.prototype.toJSON =  function () {
        var values = Object.assign({}, this.get());
        
        values.picture = config.BASE_URL + "/images/article/" + values.picture;
        return values;
    }

    return Article;
};