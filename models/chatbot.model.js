module.exports = (sequelize, Sequelize) => {
    const Chatbot = sequelize.define("chatbots", {
        base_url: {
            type: Sequelize.STRING
        },
    });
    return Chatbot;
};