module.exports = (sequelize, Sequelize) => {
    const Types = sequelize.define("types", {
        name: {
            type: Sequelize.STRING
        },
    });
    
    Types.prototype.toJSON =  function () {
        var values = Object.assign({}, this.get());
        
        return values;
    }

    return Types;
};