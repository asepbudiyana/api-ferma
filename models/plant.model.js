const config = require("../config/app.config");

module.exports = (sequelize, Sequelize) => {
    const Plant = sequelize.define("plants", {
        plant_name: {
            type: Sequelize.STRING
        },
        type_id: {
            type: Sequelize.INTEGER
        },
        category_id: {
            type: Sequelize.INTEGER
        },
        summary: {
            type: Sequelize.TEXT
        },
        growing: {
            type: Sequelize.TEXT
        },
        harvesting: {
            type: Sequelize.TEXT
        },
        picture: {
            path: Sequelize.STRING,
            type: Sequelize.STRING,
            size: Sequelize.NUMBER,
            folder: Sequelize.STRING,
            filename: Sequelize.STRING
        },
        total_days: {
            type: Sequelize.INTEGER
        },
    });
    
    Plant.prototype.toJSON =  function () {
        var values = Object.assign({}, this.get());
        
        values.picture = config.BASE_URL + "/img/" + values.picture;
        return values;
    }

    return Plant;
};