module.exports = (sequelize, Sequelize) => {
    const CropStatistic = sequelize.define("crop_statistic", {
        germ_days_low: {
            type: Sequelize.FLOAT
        },
        germ_temperature_low: {
            type: Sequelize.FLOAT
        },
        growth_days_low: {
            type: Sequelize.FLOAT
        },
        height_low: {
            type: Sequelize.FLOAT
        },
        ph_low: {
            type: Sequelize.FLOAT
        },
        spacing_low: {
            type: Sequelize.FLOAT
        },
        temperature_low: {
            type: Sequelize.FLOAT
        },
        width_low: {
            type: Sequelize.FLOAT
        },
        germ_days_up: {
            type: Sequelize.FLOAT
        },
        germ_temperature_up: {
            type: Sequelize.FLOAT
        },
        growth_days_up: {
            type: Sequelize.FLOAT
        },
        height_up: {
            type: Sequelize.FLOAT
        },
        ph_up: {
            type: Sequelize.FLOAT
        },
        spacing_up: {
            type: Sequelize.FLOAT
        },
        temperature_up: {
            type: Sequelize.FLOAT
        },
        width_up: {
            type: Sequelize.FLOAT
        },
        plant_id: {
            type: Sequelize.FLOAT
        },
    });
    
    CropStatistic.prototype.toJSON =  function () {
        var values = Object.assign({}, this.get());
        
        return values;
    }

    return CropStatistic;
};