const config = require("../config/db.config");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
    config.DB,
    config.USER,
    config.PASSWORD,
    {
        host: config.HOST,
        dialect: config.dialect,
        dialectOptions : config.dialectOptions,
        operatorsAliases: false,
        
        pool: {
            max: config.pool.max,
            min: config.pool.min,
            acquire: config.pool.acquire,
            idle: config.pool.idle
        }
    }
);  

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("../models/user.model")(sequelize, Sequelize);
db.plant = require("../models/plant.model")(sequelize, Sequelize);
db.admin = require("../models/admin.model")(sequelize, Sequelize);
db.article = require("../models/article.model")(sequelize, Sequelize);
db.activity = require("../models/activity.model")(sequelize, Sequelize);
db.myplant = require("../models/myplant.model")(sequelize, Sequelize);
db.checklist = require("../models/checklist.model")(sequelize, Sequelize);
db.crop_statistic = require("../models/crop_statistic.model")(sequelize, Sequelize);
db.category = require("../models/category.model")(sequelize, Sequelize);
db.type = require("../models/type.model")(sequelize, Sequelize);
db.checklist_pack = require("../models/checklist_pack.model")(sequelize, Sequelize);
db.chatbot = require("../models/chatbot.model")(sequelize, Sequelize);

db.article.belongsTo(db.admin, {as: 'author', foreignKey: 'adminId'});
db.article.hasMany(db.category, {foreignKey: 'article_id'});
db.category.belongsTo(db.article, {as: 'article', foreignKey: 'article_id'});
db.user.hasMany(db.myplant, {foreignKey: 'user_id'});
// db.plant.hasMany(db.myplant, {foreignKey: 'plant_id'})
db.myplant.belongsTo(db.plant, {as: 'plant', foreignKey: 'plant_id'});
db.myplant.hasMany(db.activity, {foreignKey: 'myplant_id'});
db.myplant.hasMany(db.checklist, {foreignKey: 'myplant_id'});
db.plant.hasMany(db.crop_statistic, {foreignKey: 'plant_id'});
db.crop_statistic.belongsTo(db.plant, {as: 'plant', foreignKey: 'plant_id'});
db.plant.belongsTo(db.category, {as: 'category', foreignKey: 'category_id'});
db.plant.belongsTo(db.type, {as: 'type', foreignKey: 'type_id'});
db.plant.hasMany(db.checklist_pack, {foreignKey: 'plant_id'});
db.checklist_pack.belongsTo(db.plant, {as: 'checklist_pack', foreignKey: 'plant_id'});

module.exports = db;