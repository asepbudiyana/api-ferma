"use strict";

var config = require("../config/db.config");

var Sequelize = require("sequelize");

var sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
  host: config.HOST,
  dialect: config.dialect,
  dialectOptions: {
    ssl: {
      require: config.dialectOptions.ssl.require,
      rejectUnauthorized: config.dialectOptions.ssl.rejectUnauthorized
    }
  },
  operatorsAliases: false,
  pool: {
    max: config.pool.max,
    min: config.pool.min,
    acquire: config.pool.acquire,
    idle: config.pool.idle
  }
});
var db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.user = require("../models/user.model")(sequelize, Sequelize);
db.plant = require("../models/plant.model")(sequelize, Sequelize);
db.admin = require("../models/admin.model")(sequelize, Sequelize);
db.article = require("../models/article.model")(sequelize, Sequelize);
db.activity = require("../models/activity.model")(sequelize, Sequelize);
db.myplant = require("../models/myplant.model")(sequelize, Sequelize);
db.checklist = require("../models/checklist.model")(sequelize, Sequelize);
db.article.belongsTo(db.admin, {
  as: 'author',
  foreignKey: 'adminId'
});
db.user.hasMany(db.myplant, {
  foreignKey: 'user_id'
});
db.plant.hasMany(db.myplant, {
  foreignKey: 'plant_id'
});
db.myplant.hasMany(db.activity, {
  foreignKey: 'myplant_id'
});
db.myplant.hasMany(db.checklist, {
  foreignKey: 'myplant_id'
});
db.plant.hasMany(db.crop_statistic, {
  foreignKey: 'plant_id'
});
module.exports = db;